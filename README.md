
[![Donate using Liberapay](https://liberapay.com/assets/widgets/donate.svg)](https://liberapay.com/velocitatem/donate)

# About me

I started playing around with Excel macros in about 2012, this evolved into a passion for science, math and software. Spending most of my time creating new solutions to both large and small scale problems, has lead me to explore various fields and interests, further building up my knowledge and awareness of the world.


# Currently Working on

-   Alfred

-   reorg

-   org-assistant

-   lectorg

